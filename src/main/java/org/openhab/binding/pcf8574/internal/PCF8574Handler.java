/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.pcf8574.internal;

import static org.openhab.binding.pcf8574.internal.PCF8574BindingConstants.*;

import java.io.IOException;

import org.eclipse.smarthome.config.core.Configuration;
import org.eclipse.smarthome.core.library.types.OnOffType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tinywirepi.I2C;

/**
 * The {@link PCF8574Handler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author William Chèvremont - Initial contribution
 */

public class PCF8574Handler extends BaseThingHandler {

    private final Logger logger = LoggerFactory.getLogger(PCF8574Handler.class);
    private Integer address;
    private byte state;

    private I2C m_bus;
    private String m_busDev;

    public PCF8574Handler(Thing thing) {
        super(thing);
        state = (byte) 0xff; // State at restart
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        logger.debug("Received command: {} on channelGroup {} on channel {}", command.toFullString(),
                channelUID.getGroupId(), channelUID.getIdWithoutGroup());

        byte position = 0;

        switch (channelUID.getId()) {
            case CHANNEL_P0:
                position = (byte) 0x01;
                break;
            case CHANNEL_P1:
                position = (byte) 0x02;
                break;
            case CHANNEL_P2:
                position = (byte) 0x04;
                break;
            case CHANNEL_P3:
                position = (byte) 0x08;
                break;
            case CHANNEL_P4:
                position = (byte) 0x10;
                break;
            case CHANNEL_P5:
                position = (byte) 0x20;
                break;
            case CHANNEL_P6:
                position = (byte) 0x40;
                break;
            case CHANNEL_P7:
                position = (byte) 0x80;
                break;
        }

        // Remember PCF8574 logic is reversed logic (0 = +VCC, 1=GND)
        if (command == OnOffType.OFF) {
            state |= position;
        } else if (command == OnOffType.ON) {
            state &= ~position;
        }

        // logger.info(command.toString() + " " + String.valueOf(position) + " " + channelUID.getId());

        try {
            m_bus.writeTo(address.shortValue(), state);
        } catch (IOException e) {
            logger.error(e.toString() + m_busDev + "(" + address.toString() + ")");
        }
    }

    @Override
    public void initialize() {
        // logger.debug("Start initializing!");
        updateStatus(ThingStatus.UNKNOWN);

        Configuration configuration = getConfig();
        address = Integer.parseInt((configuration.get(ADDRESS)).toString(), 16);
        m_busDev = configuration.get(BUS).toString();

        try {
            m_bus = I2C.getInstance(m_busDev);
            m_bus.writeTo(address.shortValue(), state);
            for (int i = 0; i < 8; i++) {
                updateState("P" + i, OnOffType.OFF);
            }
            updateStatus(ThingStatus.ONLINE);
        } catch (IOException e) {
            logger.error("IOException: " + e.toString());
            updateStatus(ThingStatus.OFFLINE);
        }

    }
}
