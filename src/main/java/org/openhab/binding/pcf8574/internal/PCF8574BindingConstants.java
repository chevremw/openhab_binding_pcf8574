/**
 * Copyright (c) 2010-2019 Contributors to the openHAB project
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.openhab.binding.pcf8574.internal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.ThingTypeUID;

/**
 * The {@link PCF8574BindingConstants} class defines common constants, which are
 * used across the whole binding.
 *
 * @author William Chèvremont - Initial contribution
 */
@NonNullByDefault
public class PCF8574BindingConstants {

    private static final String BINDING_ID = "pcf8574";

    // List of all Thing Type UIDs
    public static final ThingTypeUID THING_TYPE_PCF8574 = new ThingTypeUID(BINDING_ID, BINDING_ID);
    public static final String ADDRESS = "address";
    public static final String BUS = "bus";

    // List of all Channel ids
    public static final String CHANNEL_P0 = "P0";
    public static final String CHANNEL_P1 = "P1";
    public static final String CHANNEL_P2 = "P2";
    public static final String CHANNEL_P3 = "P3";
    public static final String CHANNEL_P4 = "P4";
    public static final String CHANNEL_P5 = "P5";
    public static final String CHANNEL_P6 = "P6";
    public static final String CHANNEL_P7 = "P7";
}
